#!/bin/bash
export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket
sudo dbus-uuidgen > /host/var/lib/dbus/machine-id
sudo mkdir -p /host/run/dbus
sudo ln -s /host/run/dbus /run/dbus
sudo systemctl start dbus
# Выбор временной зоны Улан-Батора
timedatectl set-timezone Asia/Ulaanbaatar
systemctl --version

# Установка и настройка NTP для локального сервера
apt install -y systemd-timesyncd
systemctl unmask systemd-timesyncd.service
systemctl enable systemd-timesyncd
systemctl start systemd-timesyncd

